<div class="container middle mt-5 mb-0">
    <div class="d-flex justify-content-center">
      <div class="card text-center">
        <div class="card-header pb-1" >
          · ¿Qué ofrecémos? ·
        </div>
        <div class="card-body">
          <h2 class="card-title pb-3" style="text-transform: uppercase; color: #3498db;">Identidad Digital</h2>
          <span ><i class="nc-icon nc-minimal-down d-inline px-3 " style="font-size: .6em; color: #ff3366;"></i></span>
          
        </div>
      </div>
    </div>
  </div>


      
 <div class="wrapper">
        

  <div class="container middle pt-0 mb-0">
    <p class="card-text text-center pt-5" >· Ventajas y Benefícios ·</p>
    <div class="d-flex justify-content-center">
      <div class="card text-center" style="border:none;">
        <div class="card-body">
          <!-- cards -->

       
          <div class="card-deck pt-2">
            <div class="card pt-5 pb-4">
              <span ><i class="nc-icon square x3 nc-spaceship" style="color: rgba(41, 128, 185,1);"></i></span>
              <div class="card-body">
                <span class="d-inline"><strong class="d-inline pr-3 pt-2 item"></strong></span>&nbsp;<h4 class="card-title d-inline item_name mr-3" style="font-weight: bold;">Alcance</h4>
                <p class="card-text item_content" data-glitch>La imagen de tu empresa en la web genera curiosidad e impacto inmediato para que el público quiera saber más.</p>
                <p class="card-text"><small class="text-muted profit">Ocupas un lugar en la mente del cliente</small></p>
              </div>
            </div>
            <div class="card pt-5 pb-4">
              <span><i class="nc-icon square x3 nc-tag-content" style="color: rgba(41, 128, 185,1);"></i></span>
              <div class="card-body">
                <span class="d-inline"><strong class="d-inline pr-3 pt-2 item"></strong></span>&nbsp;<h4 class="card-title d-inline item_name mr-3" style="font-weight: bold;">Posicionamiento</h4>
                <p class="card-text item_content" data-glitch>Tu identidad digital crea nuevas tendencias para que el consumidor sienta empatía y sea fiel a tu marca.</p>
                <p class="card-text"><small class="text-muted profit">Diferencias tu empresa de la competencia</small></p>
              </div>
            </div>
            <div class="card pt-5 pb-4">
              <span><i class="nc-icon square x3 nc-money-coins" style="color: rgba(41, 128, 185,1);"></i></span>
              <div class="card-body">
                <span class="d-inline"><strong class="d-inline pr-3 pt-2 item"></strong></span>&nbsp;<h4 class="card-title d-inline item_name mr-3" style="font-weight: bold;">Productividad</h4>
                <p class="card-text item_content" data-glitch>El público esta dispuesto a pagar un plus de marca, ya que tu identidad digital sería una garantía de calidad superior</p>
                <p class="card-text"><small class="text-muted profit">Obtienes mayores ingresos</small></p>
              </div>
            </div>
          </div>
          <!-- cards end -->
       
      </div>
    </div>
  </div>
</div>
<div class="text-center mt-5 mb-4 quote">
       <p class="mb-0 active">Tu sitio web te promueve <strong style="color: #3498db;">24/7</strong>.</p>
          <footer class="blockquote-footer"><cite title="Source Title">Krewbit</cite></footer>
</div>
</div>
 


<style>

  .wrapper{
     background: linear-gradient(to bottom, #4a69bd 50%, #000 50%);
  }

  .middle{
    color: #ecf0f1;
    font-family: 'Rajdhani',sans-serif;
   

  }
	.nc-icon{
		color: #95a5a6;
	}
	.middle .card{
		background-color: transparent;
  -webkit-transition: transform 300ms cubic-bezier(0.34, 2, 0.6, 1), box-shadow 200ms ease;
  -moz-transition: transform 300ms cubic-bezier(0.34, 2, 0.6, 1), box-shadow 200ms ease;
  -o-transition: transform 300ms cubic-bezier(0.34, 2, 0.6, 1), box-shadow 200ms ease;
  -ms-transition: transform 300ms cubic-bezier(0.34, 2, 0.6, 1), box-shadow 200ms ease;
  transition: transform 300ms cubic-bezier(0.34, 2, 0.6, 1), box-shadow 200ms ease;
		
		
	}
	
	.middle .card-deck .card{
    border:none;
    background: rgb(69,72,77);
    background: -moz-linear-gradient(top, rgba(69,72,77,1) 0%, rgba(0,0,0,1) 70%);
    background: -webkit-linear-gradient(top, rgba(69,72,77,1) 0%,rgba(0,0,0,1) 70%);
    background: linear-gradient(to bottom,rgba(69,72,77,1) 0%, rgba(0,0,0,1)  70%);
   filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#45484d', endColorstr='#000000',GradientType=0 );
   box-shadow: 10px 10px 1px rgba(0,0,0,1) ;
	}




	.middle .card-title {
    text-transform: uppercase;
    
  }

	.middle .card h4 {
		color: rgba(41, 128, 185,1);
    font-family: 'Jura', sans-serif;
	}

	.middle .text-muted.profit {
		color:#ff3366 !important;
    font-weight: bold;
	}
  
 .quote{
     
     font-family: 'Jura', sans-serif;
     font-size: .9rem;
     color: #c3c3c3;
     
  }

.middle .card-deck .card:hover{
  
  transform: translateY(-10px);
  -webkit-transform: translateY(-10px);
  -ms-transform: translateY(-10px);
  -moz-transform: translateY(-10px);

}

@media (max-width: 49em) {

   .wrapper{
     background: #000;
  }

  .middle .card-deck .card{
    background: rgb(69,72,77);
    background: -moz-linear-gradient(top, rgba(0,0,0,1) 0%, rgba(69,72,77,.3) 70%);
    background: -webkit-linear-gradient(top, rgba(0,0,0,1) 0%,rgba(69,72,77,.3) 70%);
    background: linear-gradient(to bottom,rgba(0,0,0,1) 0%, rgba(69,72,77,.3)  70%);
   filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#45484d', endColorstr='#000000',GradientType=0 );
  }


  .middle .card {
    font-size:14px;
    -o-transform: none !important;
    -moz-transform: none !important;
    -ms-transform: none !important;
    -webkit-transform: none !important;
    transform: none !important;

    -webkit-animation: none !important;
    -moz-animation: none !important;
    -o-animation: none !important;
    -ms-animation: none !important;
    animation: none !important;
  }

  .middle .card-deck .card:first-child{
    
    border-bottom-left-radius:0; 
    border-bottom-right-radius:0;

  }

    .middle .card-deck .card:last-child {

    border-top-left-radius:0; 
    border-top-right-radius:0;
    background: rgb(69,72,77);
    background: -moz-linear-gradient(top, rgba(69,72,77,.3) 0%, rgba(0,0,0,1) 100%);
    background: -webkit-linear-gradient(top, rgba(69,72,77,.3) 0%,rgba(0,0,0,1) 100%);
    background: linear-gradient(to bottom, rgba(69,72,77,.3) 0%,rgba(0,0,0,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#45484d', endColorstr='#000000',GradientType=0 );

  }

    .middle .card-deck .card:not(:first-child):not(:last-child){
      
      border-radius:0; 
      border-bottom-color: transparent;
      border-top-color: transparent;
      background: rgba(69,72,77,.3);
   
  }
  .middle .card h2 {
    font-size:22px;

	}

	.middle .card h4 {
    font-size:16px;
		color: rgba(41, 128, 185,1);
    font-family: 'Jura', sans-serif;
	}





  
}

</style>
<div class="jumbotron text-center mb-0">
  <h1 class="display-5">LA PRIMERA IMPRESION QUEDA</h1>
  <p class="lead">Tu <em style="color: #2f3542;font-weight: 600;" >Identidad Digital</em> te permite ser reconocido y tu sitio web <br> facilitarà tu proyección dentro del mercado.</p>
 <div class="row">
 <div class="col-md-3"></div>
 	 <div class="offset-md-5 col-md-6 mt-1">
  	<hr style="border: #747d8c solid 1px ;">
  </div>
 
 </div>
  <div class="my-4"></div>
  <p class="sub-header">Crea con nosotros el portal a tu empresa</p>
<div class="row justify-content-center">
	  <div class="col-md-2">
  	 <p  class="lead mb-0">
<button class="btn-lg btn-header"><a href="#" style="text-decoration: none;">· Construye tu sitio ·</a></button>
  </p>
  </div>
</div>
 
</div>
<div id="services" ></div>
<style>
	.jumbotron{
		
		  background:linear-gradient(-45deg, rgba(236,240,241,.5) 0%,rgba(236, 240, 241,1.0) 50%,rgba(255,255,255,1) 100%),url(img/bg.jpg);
		  background-size:contain;
		  background-repeat: no-repeat;
		  background-position: right;
		  color: #747d8c;
	      border-radius: 0;

		font-family: 'Jura',sans-serif;
		font-weight: 600;
		border:none;

	}
	.jumbotron h1{
		color:#2980b9;
	}
	.sub-header{
		font-family: 'Jura',sans-serif;
		color: #747d8c;
		 text-transform: uppercase;
	}
	
	.btn-header{
	
		background: rgb(69,72,77);
		background: -moz-linear-gradient(top, rgba(69,72,77,1) 0%, rgba(0,0,0,1) 100%);
		background: -webkit-linear-gradient(top, rgba(69,72,77,1) 0%,rgba(0,0,0,1) 100%);
		background: linear-gradient(to bottom, rgba(69,72,77,1) 0%,rgba(0,0,0,1) 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#45484d', endColorstr='#000000',GradientType=0 );
 	    text-transform: uppercase;
 	    font-size: .7em;
 	    font-weight: bolder;
		/*border: solid 1px #fff;*/
 	    width: 100%;
 	    font-family: 'Rajdhani',sans-serif;
	}

	.btn-header:hover{
		text-decoration: none;
		/*border: solid 1px #ff3366;
		color: #ff3366;
*/

	}
	.btn-header:hover  a{
	color: #ff3366;
	}
	@media (max-width: 49em) {

.jumbotron{
		font-size:14px;
		background:linear-gradient(-45deg, rgba(236,240,241,.5) 0%,rgba(189,195,199,1) 50%,rgba(127,140,141,1) 100%);
		background-size:contain;
		background-repeat: no-repeat;
		background-position: right;
		color: #747d8c;
		border-radius: 0;
	    font-family: 'Jura',sans-serif;
	    font-weight: 600;
	    border:none;

  }
  .jumbotron .lead{
		font-size:16px;
	
	}

  	.jumbotron h1{
		font-size:22px;
		color:#2980b9;
	}

	}

</style>
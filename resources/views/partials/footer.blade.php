<footer id="contact">
	<div class="d-flex h-100 justify-content-center text-center mx-auto">
		<div class="row w-100 align-items-center">
			<div class="col-md-4 ">
				<h6 class="d-inline">#mail</h6><span><i class="nc-icon nc-minimal-right d-inline px-3 " style="font-size: .6em; color: #ff3366;"></i></span><p class="d-inline">info@krewbit.com <strong class="d-inline bar">█</strong></p>
			</div>


			<div class="col-md-4 ">
				<a  class ="mx-auto" href="#top" style="text-decoration: none; color: #2980b9;"><h6 class="d-inline">#volver al inicio</h6><span><i class="nc-icon nc-minimal-up d-inline px-3 " style="font-size: .6em; color: #ff3366;"></i></span></a>


			</div>

			<div class="col-md-4">
				<img src="/img/brandfoot.png" alt="">
				<p class="pl-5 my-1">#digital enginnering<span><i class="nc-icon nc-minimal-left d-inline px-3 " style="font-size: .6em; color: #ff3366;"></i></span></p>
				
			</div>
		</div>
	</div>	

	<div class="row w-100 justify-content-center text-center mx-auto align-items-end">

			<p class="company">©2015-2018 Ingenieria Kinetic, C.A</p>
	</div>
</footer>

<style>
	
footer{
	height: 100%;
	font-family: 'Jura',sans-serif;
	color: #ecf0f1;

}


 .bar{
  	 animation: text-flicker 5s ease infinite;


  }

 p.company{
  	font-family: 'Michroma',sans-serif;
  	font-size: 0.7em;
  	color: #c3c3c3;
  	
  	
  }
</style>
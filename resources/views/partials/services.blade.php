<div class="d-none d-md-block d-lg-block d-xs-none" >
<div class="container bottom pt-0">
<div class="d-flex justify-content-center">
<div class="row">

       <div class="card text-center">
        <div class="card-body mb-0">
          <h2 class="card-title  pb-3" style=" text-transform: uppercase; color: #3498db;">Desarrollo de Imagen</h2>
          <span><i class="nc-icon nc-minimal-down d-inline px-3 " style="font-size: .6em; color: #ff3366;"></i></span>
        </div> 
      </div>


</div>
</div>
</div>


<div class="services-wrapper-top">
<div class="container bottom pb-5 pt-1 ">      
<div class="row mt-5 justify-content-center">
<div class="col-md-12 text-center">
     <div class="binary mt-0 mb-5" data-glitch>
                  <h4 class="card-title">· diseño ·</h4>
                  <h4 class="card-title">101010101</h4>
                  <h4 class="card-title">· design ·</h4>
</div> 
<div class="card-deck">
     
        <div class="card card-services text-center">
          <div class="card-header" >(Mockup)</div>
        <div class="card-body mt-5">
            <h4 class="card-title">Unión de Propuestas</h4>
            <p class="card-description">
              Tu imagen debe ir más allá de un logo, por eso te ayudamos a encontrar la armonia gráfica que necesitas...
          </p>
          <span style="font-size: 2.5em;"><i class="nc-icon square-light nc-layout-11 pt-3 active"></i></span>
          <p class="mt-4"  style="font-size: .9rem; color: #ff3366;">
           Cada aspecto de tu página debe sentirse como parte de un todo. Se definen las proporciones, colores y fuentes de cada elemento.
          </p>
       
   </div>
   <div class="card-footer ">
    "Autenticidad de marca es la clave del éxito"
    </div>
    </div>
  
  <br>
        <div class="card card-services text-center">
          <div class="card-header" >(UI/UX)</div>
        <div class="card-body mt-5">
            <h4 class="card-title" data-glitch>Imagen Digital</h4>
            <p class="card-description">
             Una interfaz de usuario adecuada y fluida es fundamental para capturar la atención...
         </p>
         <span style="font-size: 2.5em;"><i class="nc-icon square-light nc-tile-56"></i></span>
         <p class="pt-4"  style="font-size: .9rem; color: #ff3366;">
            Creamos la experiencia de usuario apropiada para que tu página cause la impresion deseada en los clientes que buscas.
        </p>

        
    </div>
    <div class="card-footer " style="border:none;">
        "Tu tarjeta de presentación al publico en linea"
    </div>
</div>

</div>
</div>
</div>
</div>
</div>

<hr class="my-0"  style="border:solid 0.5px #fff;">

<div class="services-wrapper-bot">
<div class="container bottom pt-1">   
    
<div class="row mt-5 justify-content-center">
  <div class="col-md-12 text-center">
     <div class="binary mt-0 mb-5" data-glitch>
                  <h4 class="card-title">· Programación ·</h4>
                  <h4 class="card-title">101010101</h4>
                  <h4 class="card-title">· Coding ·</h4>
</div>   
<div class="card-deck">
          <div class="card card-services text-center">
          <div class="card-header" >(SEO)</div>
        <div class="card-body mt-5">
            <h4 class="card-title" data-glitch>Presencia en la web</h4>
 <p class="card-description">
                           El fácil acceso a la información a través de la red vuelve indispensable poseer una Identidad Digital...
                        </p>
                         <span style="font-size: 2.5em;"><i class="nc-icon square-light nc-globe pt-3"></i></span>
                        <p class="pt-4"  style="font-size: .9rem; color: #ff3366;">
                            Nuestro trabajo no sólo implica crear una buena imagen, también nos enfocamos en hacer de tu sitio web un lugar fácil de encontrar.
                        </p>

      
   </div>
   <div class="card-footer ">
    "Tener un espacio en la red es imprescindible"
    </div>
    </div>

    <br>
            <div class="card card-services text-center">
          <div class="card-header" >Actualizacion y Migración (Framework & Database)</div>
        <div class="card-body mt-5">
            <h4 class="card-title">Ultimas Tendencias</h4>
 <p class="card-description">
                           La web evoluciona constantemente y con ello los lenguajes , estilos y tecnologias que se implementan...

                        </p>
                        
                          <span style="font-size: 2.5em;"><i class="nc-icon square-light nc-settings pt-3 active"></i></span>
                        <p  class="pt-4" style="font-size: .9rem; color: #ff3366;">
                            Tener tu sitio actualizado en codigo y diseño mantendrá la interfaz al dia con las tendencias facilitando el acceso y la utilizacion del mismo.
                        </p>

       
   </div>

   <div class="card-footer ">
    "Diseño sin funcionalidad es como un auto sin motor"
    </div>
    </div>

</div>
  </div>
</div>

  <br><br>


</div></div>
</div>



<style>

  .services-wrapper-top{
    background:linear-gradient(-45deg, rgba(74, 105, 189,1.0) 0%,rgba(74, 105, 189,1.0) 65%,rgba(74, 105, 189,.5) 100%),url(img/bg.jpg);
		  background-size:contain;
		  background-repeat: no-repeat;
		  background-position: left;
  }

   .services-wrapper-bot{
    background:linear-gradient(-45deg, rgba(74, 105, 189,.5) 0%,rgba(74, 105, 189,1.0) 35%,rgba(74, 105, 189,1.0) 100%),url(img/bg.jpg);
		  background-size:contain;
		  background-repeat: no-repeat;
		  background-position: right;
  }

  .bottom{
    color: #ecf0f1;
    font-family: 'Rajdhani',sans-serif;

  }

  .nc-icon{
    color:#6a89cc;
  }

    .bottom .card{

       background-color: transparent;
       border:none;
      -webkit-transition: transform 300ms cubic-bezier(0.34, 2, 0.6, 1), box-shadow 200ms ease;
      -moz-transition: transform 300ms cubic-bezier(0.34, 2, 0.6, 1), box-shadow 200ms ease;
      -o-transition: transform 300ms cubic-bezier(0.34, 2, 0.6, 1), box-shadow 200ms ease;
      -ms-transition: transform 300ms cubic-bezier(0.34, 2, 0.6, 1), box-shadow 200ms ease;
      transition: transform 300ms cubic-bezier(0.34, 2, 0.6, 1), box-shadow 200ms ease;
        
        
    }
    .bottom .card.card-services .card-header{
      border-bottom-color:#212529;
      background-color: #6a89cc; 
      border-radius: 5px;
    }
    
    .bottom .card.card-img{
     border:none;
     border-radius: 0;
        
    }

    .bottom .card.card-services .card-body{
          border-radius: 5px ;
          border-right: solid 1px #212529;
          background: rgb(69,72,77);
          background: -moz-linear-gradient(top, rgba(69,72,77,1) 0%, rgba(0,0,0,1) 100%);
          background: -webkit-linear-gradient(top, rgba(69,72,77,1) 0%,rgba(0,0,0,1) 100%);
          background: linear-gradient(to bottom, rgba(69,72,77,1) 0%,rgba(0,0,0,1) 100%);
          filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#45484d', endColorstr='#000000',GradientType=0 );
    }



    .bottom .card-body p{
        color: #c3c3c3;
    }
    
    .bottom .card-title {
      color: #3498db;
    } 

    .bottom .card h4 {
        color: #6a89cc;
    font-family: 'Jura', sans-serif;
    }
    
    .bottom .card-footer{
        color: #ecf0f1;
        border:none;
        background-color:transparent;
    }
    .bottom .{
        color: #ecf0f1;
       font-family: 'Jura', sans-serif; 
       font-weight: bolder;
    }

  .bottom .quote{
     
     font-family: 'Jura', sans-serif;
     font-size: .9rem;
     color: #c3c3c3;
     
  }


</style>

<div id="home" class="mobile-header d-flex justify-content-center h-100 d-none d-xs-block d-md-none d-lg-none">
	
		<div class="row align-items-center w-100">
			
				<div class="col-xs-12 mx-5">
					     <h2 class="title justify-content-center" style="color: #95a5a6;">
			              <span class="title__inner mode mode--design" data-glitch data-switch>Ingeniería-Digital<span><i class="nc-icon nc-minimal-left" style="font-size: .6em; color: #ff3366;"></i></span></span>
			            </h2>
			
					<img class="img-fluid" src="/img/brandheader.png" alt="logo" >
					    <div class="link-wrap justify-content-center"  style="color: #95a5a6; text-transform: uppercase; font-size: .8em;">
			              <a class="contact-link ">Design with us</a>
			            </div>
			            <div class="justify-content-center text-center">
			            	<span><i class="nc-icon nc-minimal-down" style="font-size: 1em; color: #ff3366;"></i></span>
			            </div>
					</div>
		</div>
		
	
</div>


<style>
	.mobile-header {
  font-family:'Jura', sans-serif;

}

	
</style>